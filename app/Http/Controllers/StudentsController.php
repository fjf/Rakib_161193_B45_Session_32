<?php

namespace App\Http\Controllers;

use App\Student;
use Illuminate\Http\Request;

class StudentsController extends Controller
{
    public function index() {

        $objStudentModel = new Student();

        $allStudents = $objStudentModel->all();

        return view('Students.index', compact('allStudents') );
    }


    public function store(){

        $objStudentModel = new Student();

        $objStudentModel->name = $_POST["name"];
        $objStudentModel->roll = $_POST["roll"];
        $objStudentModel->result = $_POST["result"];

        $status = $objStudentModel->save();

        if($status) echo "success!";
        else echo "failed";

        return redirect("students/index");


    }


    public function view($id){


        $objStudentModel = new Student();

        $student = $objStudentModel->find($id);

        return view('Students.view',compact('student'));

    }



    public function edit($id){


        $objStudentModel = new Student();

        $student = $objStudentModel->find($id);

        return view('Students.edit',compact('student'));

    }



    public function update(){

        $objStudentModel = new Student();
        $student = $objStudentModel->find($_POST["id"]);
        $student->name = $_POST["name"];
        $student->roll = $_POST["roll"];
        $student->result = $_POST["result"];

        $status = $student->update();

        if($status) echo "success!";
        else echo "failed";

        return redirect("students/index");


    }


    public function delete($id){

        $objStudentModel = new Student();

        $objStudentModel->find($id)->delete();

        return redirect("students/index");

    }















}
